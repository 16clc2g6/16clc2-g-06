#include "Header.h"

int tongSoBacBien(Don_Thuc *a)
{
	int r = 0;
	for (int i = 0; i < strlen(a->bien); i++)
	{
		r = r + a->bac_bien[i];
	}
	return r;
}



bool donThucBeHon(Don_Thuc* &a, Don_Thuc* &b)		//doi cho a->next. b->next. KHONG PHAI DOI CHO a b 
{
	if (b->next->bien == NULL)
	{
		if (a->next->bien != NULL)
			return true;
		return false;
	}
	if (a->next->bien == NULL)
		return false;

	if (tongSoBacBien(a->next) > tongSoBacBien(b->next))
		return true;
	if (tongSoBacBien(a->next) < tongSoBacBien(b->next))
		return false;

	int p = strlen(a->next->bien);		// dem bien a->next
	int q = strlen(b->next->bien);		// dem bien b->next
	if (p < q)
		return false;
	if (p > q)
	{
		return true;
	}
	if (p == q)
	{
		for (int i = 0; i < p; i++)
		{
			if (a->next->bien[i] > b->next->bien[i])
				return true;
		}
	}
	return false;
}

void chuanHoaDonThuc(Don_Thuc* &a)
{
	if (a->bien == NULL)
		return;
	int p = strlen(a->bien);		// dem bien a
	if (p < 2)
		return;
	for(int i = 0; i < p; i++)
		for (int j = i+1; j < p; j++)
			if (a->bien[j] < a->bien[i])
			{
				char t = a->bien[j];
				unsigned short int r = a->bac_bien[j];
				a->bien[j] = a->bien[i];
				a->bien[i] = t;
				a->bac_bien[j] = a->bac_bien[i];
				a->bac_bien[i] = r;
			}
}

void ChuanHoa(Da_Thuc &a)
{
	
	for (Don_Thuc *cur = a.head; cur != NULL; cur = cur->next)
		chuanHoaDonThuc(cur);

	Don_Thuc* head_tmp = new Don_Thuc;
	head_tmp->next = a.head;

	for (Don_Thuc *cur = head_tmp; cur->next != NULL; cur = cur->next)
	{
		for (Don_Thuc* tmp = cur->next; tmp != NULL; tmp = tmp->next)
		{ 
			if (tmp->next == NULL)
				break;
			if (donThucBeHon(cur, tmp) == true)
			{
				Don_Thuc* r = tmp->next;
				tmp->next = tmp->next->next;
				r->next = cur->next;
				cur->next = r;
			}
		}
	}
	a.head = head_tmp->next;
	delete head_tmp;
}