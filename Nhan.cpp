#include "Header.h"



Don_Thuc* nhanCoSo(Don_Thuc* a, Don_Thuc* b)
{
	Don_Thuc* r = new Don_Thuc;
	r->co_so = a->co_so*b->co_so;
	r->bien = NULL;
	r->bac_bien = NULL;
	return r;
}

Don_Thuc* nhanSovoiDonThuc(Don_Thuc* a, Don_Thuc* b)	//b la co_so only
{
	Don_Thuc* r = new Don_Thuc;
	r->co_so = a->co_so*b->co_so;
	r->bac_bien = new unsigned short int[strlen(a->bien)];
	r->bien = new char[strlen(a->bien)];
	for (int i = 0; i < strlen(a->bien); i++)
	{
		r->bien[i] = a->bien[i];
		r->bac_bien[i] = a->bac_bien[i];
	}
	r->bien[strlen(a->bien)] = '\0';
	return r;
}

Don_Thuc* nhanDonThucvoiDonThuc(Don_Thuc* a, Don_Thuc* b)
{
	Don_Thuc *r = new Don_Thuc;
	char* bien = new char[20];
	unsigned short int* bacbien= new unsigned short int[20];
	int sobien=0;
	r->co_so = a->co_so*b->co_so;
	int biena = strlen(a->bien);
	int bienb = strlen(b->bien);
	for (int i = 0; i < biena; i++)
	{		//tim bien giong nhau nhan lai
		for (int j = 0; j < bienb; j++)
			if (a->bien[i] == b->bien[j])
			{
				bien[sobien] = a->bien[i];
				bacbien[sobien] = a->bac_bien[i] + b->bac_bien[j];
				sobien++;
				break;
			}
	}

	for (int i = 0; i < biena; i++)										//kiem tra bien khac trong a
	{
		bool kt = 0;
		for (int j = 0; j < sobien; j++)
		{
			if (bien[j] == a->bien[i])
			{
				kt = 1;
				break;
			}
		}
		if (kt == 0)
		{
			bien[sobien] = a->bien[i];
			bacbien[sobien] = a->bac_bien[i];
			sobien++;
		}
	}

	for (int i = 0; i < bienb; i++)										//kiem tra bien khac trong b
	{
		bool kt = 0;
		for (int j = 0; j < sobien; j++)
		{
			if (bien[j] == b->bien[i])
			{
				kt = 1;
				break;
			}
		}
		if (kt == 0)
		{
			bien[sobien] = b->bien[i];
			bacbien[sobien] = b->bac_bien[i];
			sobien++;
		}
	}

	r->bac_bien = new unsigned short int[sobien];
	r->bien = new char[sobien];
	for (int i = 0; i < sobien; i++)
	{
		r->bien[i] = bien[i];
		r->bac_bien[i] = bacbien[i];
	}
	r->bien[sobien] = '\0';
	delete[]bien;
	delete[]bacbien;
	return r;
}

Don_Thuc* nhanDonThuc(Don_Thuc* a, Don_Thuc* b)
{
	Don_Thuc* r=new Don_Thuc;
	if (a->bien == NULL && b->bien == NULL)
		r = nhanCoSo(a, b);
	if (a->bien == NULL && b->bien != NULL)
		r = nhanSovoiDonThuc(b, a);
	if (a->bien != NULL&&b->bien == NULL)
		r = nhanSovoiDonThuc(a, b);
	if (a->bien != NULL && b->bien != NULL)
		r = nhanDonThucvoiDonThuc(a, b);
	return r;


}

Da_Thuc Nhan(Da_Thuc a, Da_Thuc b)
{
	Da_Thuc result;

	for(Don_Thuc* cur=a.head; cur!=NULL; cur=cur->next)
		for (Don_Thuc* tmp = b.head; tmp != NULL; tmp = tmp->next)
		{
			Don_Thuc* r = nhanDonThuc(cur, tmp);
			r->next = NULL;
			themDonThuc(result, r);
		}
	ChuanHoa(result);
	return result;
}

void themDonThuc(Da_Thuc &l, Don_Thuc* r)
{
	if (l.head == NULL)
	{
		l.head = r;
		return;
	}
	Don_Thuc* cur = l.head;
	while (cur->next != NULL)
		cur = cur->next;
	cur->next = r;
}