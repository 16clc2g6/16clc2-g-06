#include "Header.h"


void XuatHamraFile(char* s,Da_Thuc a, char* ham)
{
	fstream f;
	f.open(s, ios::app);
	f << ham << endl;
	if (a.head == NULL)
	{
		f << "None" << endl;
		return;
	}
	for (Don_Thuc* cur = a.head; cur != NULL; cur = cur->next)
	{
		if (cur->co_so > 0)
			f << "+";
		f << cur->co_so;
		if (cur->bien != NULL)
		{
			for (int i = 0; i < strlen(cur->bien); i++)
			{
				f <<"*"<< cur->bien[i];
				if (cur->bac_bien[i] != 1)
					f << "^" << cur->bac_bien[i];
			}
		}
	}
	f << endl;
	f.close();
}


void XuatraFile(Da_Thuc F1, Da_Thuc F2, Da_Thuc F3, Da_Thuc F4, Da_Thuc F5)
{
	char* s = "F-result.txt";
	fstream f;
	f.open(s, ios::out);
	if (!f.is_open())
	{
		cout << "Can't open F-result.txt !!!" << endl;
		return;
	}
	f.close();
	XuatHamraFile(s, F1, "F1");
	XuatHamraFile(s, F2, "F2");
	XuatHamraFile(s, F3, "F3");
	XuatHamraFile(s, F4, "F4");
	XuatHamraFile(s, F5, "F5");
}