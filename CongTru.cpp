#include "Header.h"

void init(Da_Thuc *&dt)
{
	dt->head = new Don_Thuc;
	dt->head = NULL;
}


//Don_Thuc* createnode(Don_Thuc x)
//{
//	Don_Thuc *p;
//	p->co_so = x.co_so;
//	if (x->bien != NULL) {
//		strcpy(p.bien, x->bien);
//		for (int i = 0; i < strlen(x->bien); i++)
//		{
//			p.bac_bien[i] = x->bac_bien[i];
//		}
//		p.next = NULL;
//		return p;
//	}
//}

void insertNode(Da_Thuc &head, Don_Thuc *pdonthuc)
{
	
	
	if (head.head == NULL)
		head.head = pdonthuc;
	else
	{ 
		Don_Thuc *cur = head.head;
		while (cur->next != NULL) 
			cur = cur->next;
		cur->next = pdonthuc;
		pdonthuc->next = NULL;
	}
}

int isuniform(Don_Thuc *dt1, Don_Thuc * dt2)
{
	if (dt1->bien == NULL && dt2->bien == NULL)
	{ 
		return 1;
	}
	else 
		if (dt1->bien == NULL || dt2->bien == NULL) 
			return 0;
	else if (strlen(dt1->bien) != strlen(dt2->bien))
		return 0;
	else
	{
		for (int i = 0; i<strlen(dt1->bien); i++)
		{
			if (dt1->bac_bien[i] != dt2->bac_bien[i] || dt1->bien[i]!= dt2->bien[i])
			{
				return 0;
			}
		}
		return 1;
	}
}

int countnode(Da_Thuc dt)
{
	int count = 0;
	if (dt.head == NULL)
	{
		cout << "empty" << endl;
	}
	else
	{
		Don_Thuc *p = dt.head;
		while (p != NULL)
		{
			p = p->next;
			count++;
		}
	}
	return count;
}

Da_Thuc add(Da_Thuc dt1, Da_Thuc dt2)
{
	int *a = new int[countnode(dt2)];
	for (int i = 0; i<countnode(dt2); i++)
	{
		a[i] = 0;
	}

	int k = 0;
	Da_Thuc dt3;
	dt3.head = NULL;
	Don_Thuc *head1 = dt1.head;
	Don_Thuc *p;
	Da_Thuc head2 = dt2;
	Don_Thuc *head3 = head2.head;
	while(head1!=NULL)
	{
		
		while (head2.head != NULL)
		{
			if (isuniform(head1, head2.head))
			{
							if(a[k]==0)
							{
								a[k]=1;
								p = new Don_Thuc;
								if(head1->bien!=NULL)
								{
									p->bac_bien = new unsigned short int[strlen(head1->bien)];
									p->bien = new char[strlen(head1->bien)];
								
									for (int i = 0; i<strlen(head1->bien); i++)
									{
										p->bac_bien[i] = head1->bac_bien[i];
									}
									strcpy(p->bien, head1->bien);
								}
								else
								{
									p->bac_bien =NULL;
									p->bien=NULL;
								}
								
								p->co_so = head1->co_so + head2.head->co_so;						
								p->next = NULL;
								insertNode(dt3, p);
								break;
							}
				}

			
			head2.head = head2.head->next;
			k++;
		
		}
		if (head2.head == NULL)
		{
			Don_Thuc *p = new Don_Thuc;
			if(head1->bien!=NULL)
			{
					p->bac_bien = new unsigned short int[strlen(head1->bien)];
					p->bien = new char[strlen(head1->bien)];
					
					for (int i = 0; i<strlen(head1->bien); i++)
					{
						p->bac_bien[i] = head1->bac_bien[i];
					}
					strcpy(p->bien, head1->bien);
			}
			else
			{
				p->bac_bien =NULL;
				p->bien=NULL;
			}
			p->co_so = head1->co_so ;
			p->next=NULL;
			insertNode(dt3, p);


		}
			head1 = head1->next;
		k = 0;
	
	}
	
	head2.head = head3;

	while (head2.head != NULL)
	{
		if (a[k] != 1)
		{
			Don_Thuc *p = new Don_Thuc;
			if(head2.head->bien!=NULL)
			{
					p->bac_bien = new unsigned short int[strlen(head2.head->bien)];
					p->bien = new char[strlen(head2.head->bien)];
					
					for (int i = 0; i<strlen(head2.head->bien); i++)
					{
						p->bac_bien[i] = head2.head->bac_bien[i];
					}
					strcpy(p->bien, head2.head->bien);
			}
			else
			{
				p->bac_bien =NULL;
				p->bien=NULL;
			}

			p->co_so = head2.head->co_so ;
			p->next=NULL;
			insertNode(dt3, p);
		}
		k++;
		head2.head = head2.head->next;
	}
	
	delete [] a;
	return dt3;
}

Da_Thuc sub(Da_Thuc dt1, Da_Thuc dt2)
{
		int *a = new int[countnode(dt2)];
	for (int i = 0; i<countnode(dt2); i++)
	{
		a[i] = 0;
	}

	int k = 0;
	Da_Thuc dt3;
	dt3.head = NULL;
	Don_Thuc *head1 = dt1.head;
	Don_Thuc *p;
	Da_Thuc head2 = dt2;
	Don_Thuc *head3 = head2.head;
	while(head1!=NULL)
	{
		
		while (head2.head != NULL)
		{
			if (isuniform(head1, head2.head))
			{
							if(a[k]==0)
							{
								a[k]=1;
								p = new Don_Thuc;
								if(head1->bien!=NULL)
								{
									p->bac_bien = new unsigned short int[strlen(head1->bien)];
									p->bien = new char[strlen(head1->bien)];
									
									for (int i = 0; i<strlen(head1->bien); i++)
									{
										p->bac_bien[i] = head1->bac_bien[i];
									}
									strcpy(p->bien, head1->bien);
								}
								else
								{
									p->bac_bien =NULL;
									p->bien=NULL;
								}
								p->co_so = head1->co_so - head2.head->co_so;
								p->next = NULL;
								insertNode(dt3, p);
								break;
							}
						
				
				}

			
			head2.head = head2.head->next;
			k++;
		
		}
		if (head2.head == NULL)
		{
			Don_Thuc *p = new Don_Thuc;
			if(head1->bien!=NULL)
			{
					p->bac_bien = new unsigned short int[strlen(head1->bien)];
					p->bien = new char[strlen(head1->bien)];
					
					for (int i = 0; i<strlen(head1->bien); i++)
					{
						p->bac_bien[i] = head1->bac_bien[i];
					}
					strcpy(p->bien, head1->bien);
			}
			else
			{
				p->bac_bien =NULL;
				p->bien=NULL;
			}
			p->co_so = head1->co_so ;
			p->next=NULL;
			insertNode(dt3, p);


		}
			head1 = head1->next;
		k = 0;
	
	}
	
	head2.head = head3;

	while (head2.head != NULL)
	{
		if (a[k] != 1)
		{
			Don_Thuc *p = new Don_Thuc;
			if(head2.head->bien!=NULL)
			{
					p->bac_bien = new unsigned short int[strlen(head2.head->bien)];
					p->bien = new char[strlen(head2.head->bien)];
					
					for (int i = 0; i<strlen(head2.head->bien); i++)
					{
						p->bac_bien[i] = head2.head->bac_bien[i];
					}
					strcpy(p->bien, head2.head->bien);
			}
			else
			{
				p->bac_bien =NULL;
				p->bien=NULL;
			}

			p->co_so = head2.head->co_so ;
			p->next=NULL;
			insertNode(dt3, p);
		}
		k++;
		head2.head = head2.head->next;
	}
	
	delete [] a;
	return dt3;
}