#include "Header.h"

void main()
{

	Da_Thuc F1;		//doc file
	Da_Thuc F2;		//doc file
	Da_Thuc F3;		//F1 + F2
	Da_Thuc F4;		//F1 - F2
	Da_Thuc F5;		//F1.F2

//1. doc file F1 F2
	readFromFile("F1.txt", F1);				// test F3, F4. Chay chuogn trinh thi F1, F2
	readFromFile("F2.txt", F2);


//3. Rut Gon
	Rut_Gon(F1);
	Rut_Gon(F2);

//Chuan hoa
	ChuanHoa(F1);
	ChuanHoa(F2);

//4. Cong
	F3 = add(F1, F2);
	Rut_Gon(F3);

//5. Tru
	F4 = sub(F1, F2);
	Rut_Gon(F4);

//6/ Nhan
	F5 = Nhan(F1, F2);
	Rut_Gon(F5);

//7. in man hinh
	cout << "F1: " << endl;
	printDaThuc(F1);
	cout << endl << endl;
	cout << "F2: " << endl;
	printDaThuc(F2);
	cout << endl << endl;
	cout << "F3 = F1 + F2 = " << endl;
	printDaThuc(F3);
	cout << endl << endl;
	cout << "F4 = F1 - F2 =" << endl;
	printDaThuc(F4);
	cout << endl << endl;
	cout << "F5 = F1.F2 = " << endl;
	printDaThuc(F5);
	cout << endl << endl;

//8. Xuat ra File
	XuatraFile(F1, F2, F3, F4, F5);
	cin.get(); //Dung chuong trinh
}