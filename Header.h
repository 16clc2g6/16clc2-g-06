#pragma once
#ifndef _HEADER_H_
#define _HEADER_H_
#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
using namespace std;

struct Don_Thuc
{
	float co_so;
	char *bien;
	unsigned short int *bac_bien;
	Don_Thuc* next;
};

struct Da_Thuc
{
	Don_Thuc* head=NULL;
};


//MINH THE (NHAP XUAT)
bool readFromFile(string filename, Da_Thuc &F);
Da_Thuc splitChuoiDaThucTuFile(fstream &file);
Don_Thuc splitNhungDonThucDauTien(string &A, size_t &pos, size_t &prev_pos);
Don_Thuc splitDonThucCuoiCungTuFile(string &A, size_t &prev_pos);
bool khongCoCoSo(string B);
bool khongCoBien(istringstream &ss);
int demSoBien(string token);
void printDonThuc(Don_Thuc X);
bool addDonThuc(Da_Thuc &DaThuc, Don_Thuc DonThuc);
void printDaThuc(Da_Thuc DaThuc);
bool outputResult(Da_Thuc A);



//BA TUAN (CONG TRU)
int countnode(Da_Thuc dt);
void init(Da_Thuc *&dt);
//Don_Thuc createnode(Don_Thuc *x);
void insertNode(Da_Thuc &head, Don_Thuc *pdonthuc);
int isuniform(Don_Thuc *dt1, Don_Thuc * dt2);
Da_Thuc add(Da_Thuc dt1, Da_Thuc dt2);
Da_Thuc sub(Da_Thuc dt1, Da_Thuc dt2);
//void output(Da_Thuc dt);


//ANH QUAN (RUT GON)
bool KThaidonthuc(Don_Thuc *&a, Don_Thuc *&b);
void Rut_Gon(Da_Thuc &dathuc);


//VAN TUAN (NHAN)
Don_Thuc* nhanCoSo(Don_Thuc* a, Don_Thuc* b);
Don_Thuc* nhanSovoiDonThuc(Don_Thuc* a, Don_Thuc* b);
Don_Thuc* nhanDonThucvoiDonThuc(Don_Thuc* a, Don_Thuc* b);
Don_Thuc* nhanDonThuc(Don_Thuc* a, Don_Thuc* b);
Da_Thuc Nhan(Da_Thuc a, Da_Thuc b);
void themDonThuc(Da_Thuc &l, Don_Thuc* r);


//VAN NGOC (CHUAN HOA) - VAN TUAN LAM LUON ROI ~~
int tongSoBacBien(Don_Thuc* a);
bool donThucBeHon(Don_Thuc* &a, Don_Thuc* &b);
void chuanHoaDonThuc(Don_Thuc* &a);
void ChuanHoa(Da_Thuc &a);


//JUST_IN_CASE
void XuatHamraFile(char* s, Da_Thuc a, char* ham);
void XuatraFile(Da_Thuc F1, Da_Thuc F2, Da_Thuc F3, Da_Thuc F4, Da_Thuc F5);

#endif // !HEADER_H
