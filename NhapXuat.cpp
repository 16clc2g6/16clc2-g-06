#include "Header.h"

bool readFromFile(string filename, Da_Thuc &F) {
	fstream file;
	file.open(filename, ios::in);

	if (file.is_open()) {
		F = splitChuoiDaThucTuFile(file);
		file.close();
		return true;
	}
	else {
		cout << "Can't open " << filename << endl;
		file.close();
		return false;
	}
}

Da_Thuc splitChuoiDaThucTuFile(fstream &file) {
	string A;
	getline(file, A);
	Da_Thuc F;
	F.head = NULL;
	Don_Thuc X;

	size_t prev_pos = 0;
	size_t pos;

	while ((pos = A.find_first_of("+-", prev_pos)) != A.npos) {
		if (pos == 0) {
			prev_pos++;
			pos = A.find_first_of("+-", prev_pos);
		}
		X = splitNhungDonThucDauTien(A, pos, prev_pos);
		addDonThuc(F, X);
	}

	//Prev_pos luc nay se = Vi tri cua co so cua don thuc cuoi cung
	X = splitDonThucCuoiCungTuFile(A, prev_pos);
	addDonThuc(F, X);

	return F;
}

Don_Thuc splitNhungDonThucDauTien(string &A, size_t &pos, size_t &prev_pos) {
	//Bat dau tim trong A tu vi tri prev_pos den khi nao gap ki tu "+" or "-"
	//Tra ve vi tri (pos) do
	//Thuc hien while khi pos khac vi tri "the end of string" (npos)
	Don_Thuc X;
	//INIT(X);

	string B;
	if (pos > prev_pos) {
		if (prev_pos != 0) //Neu prev_pos khac 0 thi lui ve sau 1 vi tri de lay luon dau "+" or "-"
			prev_pos--;

		B = A.substr(prev_pos, pos - prev_pos); //Cat ra 1 don thuc tu da thuc. VD: 2*A^3*B + 0*B se duoc 2*A^3*B va 0*B
												//cout << B << endl;
		istringstream ss(B);
		string token;

		//TH don thuc khong co co so dung o dau da thuc. VD: a*b
		if (prev_pos == 0) {
			int tmp = B.find_first_of("abcdefghijklmnopqrstuvwxyz", 0);
			if (tmp == 0)
				X.co_so = 1;
			else if (tmp == 1)
				X.co_so = -1;
			else if (tmp > 1)
				goto label3;
			goto label2;
		}

		//TH chi co bien ma khong co co so. VD: a*b*c => Co so = 1;
		if (khongCoCoSo(B) == true) {
			if (B[0] == '-')
				X.co_so = -1;
			else if (B[0] = '+')
				X.co_so = 1;
		}
		else {
		label3:;
			getline(ss, token, '*'); //Cat ra co so o dang string. VD: 2*A^3*B se duoc 2
			istringstream _coso(token); //Chuyen string thanh float
			_coso >> X.co_so; //Day vao bien coso;
		}

		//Neu Don thuc chi co co so ma khong co bien thi nhay toi "label1"
		if (!khongCoCoSo(B) && khongCoBien(ss)) {
			X.bien = NULL;
			X.bac_bien = NULL;
			goto label1;
		}
		else {
		label2:;
			getline(ss, token); //Cat ra phan bien va mu. VD: 2*A^3*B se duoc A^3*B 

								//Neu roi vao TH khong co co so thi dau truoc chuoi B se van duoc giu nguyen. VD: -c
								//Do do de duyet bien va bac bien can phai cat bo di dau +/- nay => c
			if (token[0] == '+' || token[0] == '-')
				token = token.substr(1, token.length() - 1);

			int so_bien = demSoBien(token) + 1; //So luong Bien trong Don Thuc
			X.bien = new char[so_bien + 1];
			X.bac_bien = new unsigned short int[so_bien];
			X.bien[so_bien] = '\0';

			//Cat ra tung phan bien va mu. VD: A^3*B se duoc A^3 va B
			istringstream tt(token);
			for (int j = 0; j < so_bien; j++) {
				string token2;
				getline(tt, token2, '*');

				//Neu chieu dai token2 = 1 => Token 2 chinh la bien. VD: A (A^1)
				if (token2.length() == 1) {
					X.bien[j] = token2[0];
					X.bac_bien[j] = 1;
					continue;
				}

				istringstream uu(token2);
				string _bien;
				getline(uu, _bien, '^');
				X.bien[j] = _bien[0];

				string _bacbien;
				getline(uu, _bacbien);
				istringstream _bacbien_(_bacbien);
				_bacbien_ >> X.bac_bien[j];
			}
		}
	label1:;
		//printDonThuc(X);
		//cout << endl;
	}
	prev_pos = pos + 1;
	return X;
}

Don_Thuc splitDonThucCuoiCungTuFile(string &A, size_t &prev_pos) {
	Don_Thuc X;
	//INIT
	if (prev_pos < A.length()) {
		string B;
		if (prev_pos != 0)
			prev_pos--;

		B = A.substr(prev_pos, A.length() - prev_pos);

		istringstream ss(B);
		string token;

		//Truong hop chi co bien ma khong co co so. VD: a*b*c => Co so = 1;
		if (khongCoCoSo(B) == true) {
			if (B[0] == '+')
				X.co_so = 1;
			else if (B[0] == '-')
				X.co_so = -1;
		}
		else {
			getline(ss, token, '*'); //Cat ra co so o dang string. VD: 2*A^3*B se duoc 2
			istringstream _coso(token); //Chuyen string thanh float
			_coso >> X.co_so; //Day vao bien coso;
							  //cout << X->co_so << endl;
		}

		//Neu Don thuc chi co co so ma khong co bien thi nhay toi "label1"
		if ((khongCoCoSo(B) == false) && (khongCoBien(ss) == true)) {
			X.bien = NULL;
			X.bac_bien = NULL;
			goto label2;
		}
		else {
			getline(ss, token); //Cat ra phan bien va mu. VD: 2*A^3*B se duoc A^3*B 

								//Neu roi vao TH khong co co so thi dau truoc chuoi B se van duoc giu nguyen. VD: -c
								//Do do de duyet bien va bac bien can phai cat bo di dau +/- nay => c
			if (token[0] == '+' || token[0] == '-')
				token = token.substr(1, token.length() - 1);

			int so_bien = demSoBien(token) + 1; //So luong Bien trong Don Thuc
			X.bien = new char[so_bien + 1];
			X.bac_bien = new unsigned short int[so_bien];
			X.bien[so_bien] = '\0';

			//Cat ra tung phan bien va mu. VD: A^3*B se duoc A^3 va B
			istringstream tt(token);
			for (int j = 0; j < so_bien; j++) {
				string token2;
				getline(tt, token2, '*');
				//cout << token2 << endl;

				//Neu chieu dai token2 = 1 => Token 2 chinh la bien. VD: A (A^1)
				if (token2.length() == 1) {
					X.bien[j] = token2[0];
					X.bac_bien[j] = 1;
					continue;
				}

				istringstream uu(token2);
				string _bien;
				getline(uu, _bien, '^');
				X.bien[j] = _bien[0];

				string _bacbien;
				getline(uu, _bacbien);
				istringstream _bacbien_(_bacbien);
				_bacbien_ >> X.bac_bien[j];
			}
		}
	label2:;
		//printDonThuc(X);
		//cout << endl;
	}
	return X;
}

void printDonThuc(Don_Thuc X) {
	//cout << X.co_so << endl;
	if (X.co_so > 0)
		cout << "+" << X.co_so;
	else
		cout << X.co_so;
	if (X.bien != NULL && (X.bien) > 0) {
		for (int i = 0; i < strlen(X.bien); i++) {
			cout << "*" << X.bien[i];
			if (X.bac_bien[i] > 1)
				cout << "^" << X.bac_bien[i];
		}
	}
	cout << " ";
}

bool khongCoCoSo(string B) {
	size_t tmp_pos = -1;
	tmp_pos = B.find_first_of("abcdefghijklmnopqrstuvwxyz", 0);
	if (tmp_pos == 1) {
		return true;
	}
	return false;
}

int demSoBien(string token) {
	//Dem so toan tu '*' trong chuoi token
	//So bien = So toan tu '*' + 1;
	int multiplyOperator = 0;
	int tokenLength = token.length();

	for (int i = 0; i < tokenLength; i++) {
		if (token[i] == '*')
			multiplyOperator++;
	}
	return multiplyOperator++;
}

bool khongCoBien(istringstream &ss) {
	int posOfss = ss.tellg(); //Lay vi tri hien tai cua con trong stringstream.
	if (posOfss == -1) //Neu con tro = -1 => Da duyet het chuoi => Ko co phan tu => Don thuc chi co co so
		return true;
	return false;
}

bool addDonThuc(Da_Thuc &DaThuc, Don_Thuc DonThuc) {
	bool result = false;

	//XET TRUONG HOP DON THUC KHONG TON TAI

	Don_Thuc *p = new Don_Thuc;
	p->co_so = DonThuc.co_so;

	if (DonThuc.bien == NULL) {
		p->bac_bien = NULL;
		p->bien = NULL;
	}
	else {
		int so_bien = strlen(DonThuc.bien);
		p->bien = new char[so_bien + 1];
		p->bac_bien = new unsigned short int[so_bien + 1];

		p->bien[so_bien] = '\0';
		p->bac_bien[so_bien] = '\0';

		for (int i = 0; i < so_bien; i++) {
			p->bien[i] = DonThuc.bien[i];
			p->bac_bien[i] = DonThuc.bac_bien[i];
		}
	}
	p->next = NULL;

	if (DaThuc.head == NULL) {
		result = true;
		DaThuc.head = p;
	}
	else {
		result = true;
		Don_Thuc *cur = DaThuc.head;
		while (cur->next != NULL) {
			cur = cur->next;
		}
		cur->next = p;
	}
	return result;
}

void printDaThuc(Da_Thuc DaThuc) {
	if (DaThuc.head == NULL)
		cout << "No data" << endl;
	else {
		Don_Thuc *cur = DaThuc.head;
		while (cur != NULL) {
			printDonThuc(*cur);
			cur = cur->next;
		}
	}
}

bool outputResult(Da_Thuc A) {
	fstream file;
	file.open("F-result.txt", ios::app);

	if (file.is_open())
	{

		string tmp;
		int count = 0;
		while (!file.eof()) {
			getline(file, tmp);
			count++;
		}
		cout << count << endl;

		file.close();
		return true;
	}
	else {
		cout << "Can't open F-result.txt " << endl;
		file.close();
		return false;
	}

}

