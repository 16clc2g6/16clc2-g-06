#include "Header.h"

bool KThaidonthuc(Don_Thuc *&a, Don_Thuc *&b)
{
	if (a->bien == NULL && b->bien == NULL)
		return true;
	if (a->bien != NULL&&b->bien != NULL)
	{
		if (strlen(a->bien) == strlen(b->bien))
		{
			for (int i = 0; i < strlen(a->bien); i++)
				if (a->bien[i] != b->bien[i] || a->bac_bien[i] != b->bac_bien[i])
					return false;
			return true;
		}
	}
	return false;
}

void Rut_Gon(Da_Thuc &dathuc)
{
	for (Don_Thuc* cur = dathuc.head; cur != NULL; cur = cur->next)
		for (Don_Thuc* tmp = cur; tmp != NULL; tmp = tmp->next)
		{
			if (tmp->next == NULL)
				break;
			if (KThaidonthuc(cur, tmp->next) == true)
			{
				cur->co_so = cur->co_so + tmp->next->co_so;
				Don_Thuc *r = tmp->next;
				tmp->next = tmp->next->next;
				delete r;
			}
		}
	Don_Thuc* tmp = new Don_Thuc;
	tmp->next = dathuc.head;
	for (Don_Thuc* cur = tmp; cur->next != NULL; cur = cur->next)
	{
		if (cur->next->co_so == 0)						
		{
			Don_Thuc* r = cur->next;
			cur->next = cur->next->next;
			delete r;
		}
	}
	dathuc.head = tmp->next;
	delete tmp;
}
